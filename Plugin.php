<?php
/**
 * Move Floating Social Bar in Genesis 在创世纪中移动浮动社交栏
 *
 * @package   Move_Floating_Social_Bar_In_Genesis 在创世纪中移动社交栏
 * @author    Gary Jones <gary@garyjones.co.uk> 作者 Gary Jones
 * @license   GPL-2.0+ 授权 GPL-2.0+
 * @link      https://github.com/GaryJones/move-floating-social-bar-in-genesis 连接 https://github.com/GaryJones/move-floating-social-bar-in-genesis
 * @copyright 2013 Gary Jones,  版权所有 2013 Gary Jones
 *
 * @wordpress-plugin 
 * Plugin Name:       Move Floating Social Bar in Genesis 插件名：在创世纪中移动社交栏
 * Plugin URI:        https://github.com/GaryJones/move-floating-social-bar-in-genesis 插件URI
 * Description:       Moves the Floating Social Bar plugin output from just inside the entry content to just before it.将浮动社交栏插件输出从条目内容内部移动到条目内容之前
 * Version:           1.0.0 版本 1.0.0
 * Author:            Gary Jones 作者 Gary Jones
 * Author URI:        https://github.com/GaryJones/move-floating-social-bar-in-genesis 作者URI
 * Text Domain:       move-floating-social-bar-in-genesis 文字域 move-floating-social-bar-in-genesis
 * License:           GPL-2.0+ 授权 GPL2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt 授权URI
 * Domain Path:       /languages 域名路径/languages
 * GitHub Plugin URI: https://github.com/GaryJones/move-floating-social-bar-in-genesis
 * GitHub Branch:     master    Github分支 master
 */

// If this file is called directly, abort. 如果这个文件直接被调用，则终止
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action( 'pre_get_posts', 'mfsbig_remove_floating_social_bar', 15 );
/**
 * Remove Floating Social Bar from outputting at the top of the content.
 	从内容顶部的输出中删除Floating Social Bar
 *
 * FSB adds these filters at pre_get_posts, priorty 10, so we'll remove them
   FSB将这些过滤器添加到pre_get_posts（优先级为10），因此我们将其删除
   
 * just after that. 在那之后
 *
 * @since 1.0.0
 */
function mfsbig_remove_floating_social_bar() {
	if ( class_exists( 'floating_social_bar' ) ) {
		//如果有'floating_social_bar'的类存在
		
		remove_filter( 'the_excerpt', array( floating_social_bar::get_instance(), 'fsb' ), apply_filters( 'fsb_social_bar_priority', 10 ) );
		remove_filter( 'the_content', array( floating_social_bar::get_instance(), 'fsb' ), apply_filters( 'fsb_social_bar_priority', 10 ) );
	}
}

add_action( 'genesis_before_post_content', 'mfsbig_add_floating_social_bar' );  // XHTML themes
add_action( 'genesis_before_entry_content', 'mfsbig_add_floating_social_bar' ); // HTML5 themes
/**
 * Echo the Floating Social Bar in the right place, just before the entry
  在进入入口之前，在正确的地方呼应浮动的社交酒吧
 
 * content (after the header and entry meta) in Genesis child themes.
  内容 (在标题和条目元之间)在“创世纪”的儿童主题中
 *
 * As fsb() is really a function for filtering, it requires a single argument
  fsb是一个过滤的功能，它需要一个单独的参数
 * (the content or excerpt), so we fool it by passing in an empty string instead.
 * 内容或摘录,所以我们传递一个空的字符串来代替它。
 * @since 1.0.0
 */
function mfsbig_add_floating_social_bar() {
	if ( class_exists( 'floating_social_bar' ) ) {
		echo floating_social_bar::get_instance()->fsb('');
	}
}
